# Introduction

{{#include ../README.md:badges}}

**If comming from v2, please check [v3 breaking changes](./v3.md).**

**wallust** is a command line tool for creating 16 color palettes, since it was the original intent of [_pywal_](https://github.com/dylanaraps/pywal), the tool that [inspired the creation of wallust](https://github.com/dylanaraps/pywal/issues/701).

![gif](https://explosion-mental.codeberg.page/img/other/wallust-2.6.gif "wallust gif")

{{#include ../README.md:feats}}
