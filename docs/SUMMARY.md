# Summary

[Introduction](./intro.md)

# Guide
- [Installation](./installation/README.md)
    - [Distribution Packages](./installation/distro.md)
    - [Building from source](./installation/src.md)

# Reference
- [Migration to v3 Guide](./v3.md)

- [Parameters](./parameters/README.md)
    - [Alpha](./parameters/alpha.md)
    - [Backend](./parameters/backend.md)
    - [Check Contrast](./parameters/check_contrast.md)
    - [Color Space](./parameters/colorspace.md)
    - [Fallback Generator](./parameters/fallback_generator.md)
    - [Palette](./parameters/palette.md)
    - [Saturation](./parameters/saturation.md)
    - [Threshold](./parameters/threshold.md)

- [Configuration File](./config/README.md)
    - [Defining a Template](./config/template.md)
    - [Example](./config/example.md)

- [Templates](./templates/README.md)
    - [Syntax](./templates/syntax.md)
    - [Variables](./templates/variables.md)
    - [Filters](./templates/filters.md)
    - [Example](./templates/example.md)
    - [Pywal compatibility](./templates/pywal.md)

# Dev
- [Contribute!](./contribute.md)
- [Packaging](./packaging.md)
    - [Makefile](./packaging-make.md)

[Screenshots](./ss.md)
