# Example

```
# zathurarc config sample
...

# colors
set default-bg     "{{background}}"
set default-fg     "{{foreground}}"

# make it a bit lighter than background
set statusbar-bg   "{{background | lighten(0.3)}}"

# make it darken by blending to a darken color
set statusbar-fg   "{{foreground | blend("#eeeeee")}}"

# use it's complementary
set inputbar-bg    "{{background | complementary}}"
```

Then you can add this file to `~/.config/wallust/templates`
and use the config file to **template** it. For example,
`zathura.template = 'zathurarc'`, and then define a
**target** field, see [config](../config/README.md).
