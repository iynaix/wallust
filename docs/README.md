This is an [`mdbook`](https://rust-lang.github.io/mdBook) _book_. Some info may
be fetch from the `readme` directly, to avoid contraditory statments while
redacting documentation. So if you see placeholders/macros inside `{{}}`
(handlebars variables) because of that.

The assets from this repo are at the [`pages`](https://codeberg.org/explosion-mental/wallust/src/branch/pages) branch.

You can check the page at <https://explosion-mental.codeberg.page/wallust>,
which serves as a friendly guide to those who don't like man pages :)
