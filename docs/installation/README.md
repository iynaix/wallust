# Installation

*wallust* can run on Linux, BSDs, MacOS and Windows.

{{#include ../README.md:repology}}

- For Linux, first check if your [distro already has it packaged](installation-distro.md).
- You can also check out [Building from source](installation-src.md), to compile the program.


## Binary packages
Go to the [releases](https://codeberg.org/explosion-mental/wallust/releases)
and download the `tar.gz` file.
- you have a binary for musl, so it works for most *nix platforms, or
- a `.exe` for Windows users.

```
tar -xf wallust-TARGET.tar.gz
```

