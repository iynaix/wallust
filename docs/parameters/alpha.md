# Alpha

Alpha value for templating (default: **100**).

This value doesn't do anything other than represent the variable called `alpha`
in templates. This is simply a left over of niche use cases.

<hr>

To edit this value:
- **Config file**: `alpha = 50`
- **Cli**: `wallust run image.png --alpha 50`
