| Palette | Description |
|---------|-------------|
**Dark** | 8 dark colors, dark background and light contrast
**Dark16** | Same as `dark` but uses the 16 colors trick
**DarkComp** | This is a `dark` variant that changes all colors to it's complementary counterpart, giving the feeling of a 'new palette' but that still makes sense with the image provided.
**DarkComp16** | 16 variation of the dark complementary variant
**AnsiDark** | This is not a 'dark' variant, is a new palette that is meant to work with `lchansi` colorspace, which will maintain 'tty' like color order and only adjusting the colors acording to the theme. A possible solution for LS_COLORS and the like. Should workout with other colorspace, but the result may not be optimal.
**HardDark** | Same as `dark` with hard hue colors
**HardDark16** | Harddark with 16 color variation
**HardDarkComp** | complementary colors variation of harddark scheme
**HardDarkComp16** | complementary colors variation of harddark scheme
**Light** | Light bg, dark fg
**Light16** | Same as `light` but uses the 16 color trick
**LightComp** | complementary colors variation of light
**LightComp16** | complementary colors variation of light with the 16 color variation
**SoftDark** | Variant of softlight, uses the lightest colors and a dark background (could be interpreted as `dark` inversed)
**SoftDark16** | softdark with 16 color variation
**SoftDarkComp** | complementary variation for softdark
**SoftDarkComp16** | complementary variation for softdark with the 16 color variation
**SoftLight** | Light with soft pastel colors, counterpart of `harddark`
**SoftLight16** | softlight with 16 color variation
**SoftLightComp** | softlight with complementary colors
**SoftLightComp16** | softlight with complementary colors with 16 colors
