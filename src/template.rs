//! Template stuff, definitions and how it's parsed
use std::fs::read_to_string;
use std::path::Path;
use std::collections::HashMap;
use std::str::FromStr;

use crate::{
    colors::{ Colors, Myrgb },
    config::Fields,
    palettes::Palette,
    backends::Backend,
    colorspaces::ColorSpace,
};

use anyhow::Result;
use owo_colors::OwoColorize;
use minijinja::{Environment, context};
use minijinja::value::ViaDeserialize;

use palette::{
    Darken, Lighten, IntoColor, Saturate,
    Srgb, Srgba, Hsv,
};

pub struct TemplateFields<'a> {
    pub alpha: u8,
    pub backend: &'a Backend,
    pub palette: &'a Palette,
    pub colorspace: &'a ColorSpace,
    pub image_path: &'a str,
    pub colors: &'a Colors,
}

macro_rules! jinjafn {
    ($var:expr, $func_name:ident) => {
        fn $func_name(value: ViaDeserialize<Myrgb>) -> String { Myrgb::$func_name(&value) }
        $var.add_filter(stringify!($func_name), $func_name);
    };
    ($var:expr, tostr => $func_name:ident) => {
        fn $func_name(value: ViaDeserialize<Myrgb>) -> String { Myrgb::$func_name(&value).to_string() }
        $var.add_filter(stringify!($func_name), $func_name);
    };

    ($var:expr, $func_name:ident, $arg:ty) => {
        fn $func_name(value: ViaDeserialize<Myrgb>, other: $arg) -> String { Myrgb::$func_name(&value, other) }
        $var.add_filter(stringify!($func_name), $func_name);
    };
    ($var:expr, tostr => $func_name:ident, $arg:ty) => {
        fn $func_name(value: ViaDeserialize<Myrgb>, other: $arg) -> String { Myrgb::$func_name(&value, other).to_string() }
        $var.add_filter(stringify!($func_name), $func_name);
    };
    ($var:expr, $func_name:ident, deref => $arg:ty) => {
        fn $func_name(value: ViaDeserialize<Myrgb>, other: $arg) -> String { Myrgb::$func_name(&value, *other) }
        $var.add_filter(stringify!($func_name), $func_name);
    };
    ($var:expr, tostr => $func_name:ident, deref => $arg:ty) => {
        fn $func_name(value: ViaDeserialize<Myrgb>, other: $arg) -> String { Myrgb::$func_name(&value, *other).to_string() }
        $var.add_filter(stringify!($func_name), $func_name);
    };
}

/// Recommended way to chain errors
/// ref: <https://docs.rs/minijinja/latest/minijinja/struct.Error.html>
fn minijinja_err_chain(err: minijinja::Error) -> String {
    let mut err = &err as &dyn std::error::Error;
    let mut s = String::from(&format!("Could not render template: {err:#}"));

    // get to the source, if there are more.
    while let Some(next_err) = err.source() {
        s.push('\n');
        s.push_str(&format!("Caused by: {next_err:#}"));
        err = next_err;
    }
    s
}

/// Render the template `file` provided and write it to `target_path`.
/// `.map_err` is used to append friendly "Reading 'file' failed" or the like,
/// since we don't care about handling all possible io::Errors
// TODO there's gonna be trouble harcoding:
// //with jinja:
// let mut env = Environment::new(); //preload jinja enviroment
// ... // set jinja functions
// file_render(&env, ..) //pass env as reference,
// //with pywal(new_string):
// let values = values.to_hash(..); //preload hashmap, so it doesn't create a new one for every iteration
// file_render(&values, ..);
//
// Maybe a solution is something like:
// let test = templates.iter().any(|x| x.pywal == Some(true));
// Then create env or the hasmap as an option, and `.expect` to open it an pass it as file_render():
// 1. If it's None, it will never reach expect.
// 2. If it's Some, it will always be true an a valid value.
pub fn file_render(env: &mut Environment, file: &Path, target_path: &Path, pywal: bool, values: &TemplateFields) -> Result<(), String> {
    let filename = file.display();
    let filename = filename.italic();

    let file_content = read_to_string(file)
        .map_err(|err| format!("Reading {filename} failed: {err}"))?;

    // First find if the parent exists at all before rendering
    match target_path.parent() {
       Some(s) => std::fs::create_dir_all(s)
           .map_err(|err| format!("Failed to create parent directories from {}: {err}", target_path.display().italic()))?,
       None => return Err(format!("Failed to find file parent from {}", target_path.display().italic())),
    };

    // Template/render the file_contents
    let rendered = if ! pywal {
        jinja_update_alpha(env, values.alpha);
        let name = file.display().to_string();
        let v = minijinja::Value::from(values);

        //env.add_template(&name, &file_content);
        // env.add_template_owned(name, file_content).map_err(minijinja_err_chain)?;
        //
        // let t = env.get_template(&file.display().to_string()).map_err(minijinja_err_chain)?;
        // t.render(v)

        env.render_named_str(&name, &file_content, v)
            .map_err(minijinja_err_chain)?
    } else {
        new_string_template::template::Template::new(file_content)
            // this regex is even better than pywal, doesn't match new lines :3
            // <https://regex101.com/r/AgVXKJ/1>
            .with_regex(&regex::Regex::new(r"\{(\S+?)\}").expect("correct tested regex"))
            .render(&values.to_hash())
            .map_err(|err| format!("Error while rendering '{filename}': {err}"))?
    };

    // map io::Errors into a writeable one (String) ((maybe this is how anyhow werks?))
    std::fs::write(target_path, rendered)
        .map_err(|err| format!("Error while writting to {}: {err}", target_path.display()))
}

/// Writes `template`s into `target`s. Given the many possibilities of I/O errors, template errors,
/// user typos, etc. Most errors are reported to stderr, and ignored to `continue` with the other
/// entries.
pub fn write_template(config_dir: &Path, templates_header: &HashMap<String, Fields>, values: &TemplateFields, quiet: bool) -> Result<()> {

    let mut jinjaenv = jinja_env();
    //XXX loader makes avaliable the (easy) use of `import` and such
    jinjaenv.set_loader(minijinja::path_loader(config_dir));


    // iterate over contents and pass it as an `&String` (which is casted to &str), apply the
    // template and write the templated(?) file to entry.path

    for (name, fields) in templates_header {
        // facilitates strings printing
        let name = name.bold();
        let target = &fields.target.italic();
        let warn = "W".red();
        let warn = warn.bold();

        //root path for the template file
        let path = config_dir.join(&fields.template);

        //root path for the target file (requires interpret `~` for home)
        //XXX on `shellexpand`, think about using `::full()` to support env vars. Seems a bit sketchy/sus
        let env = shellexpand::tilde(&fields.target);
        let target_path = Path::new(env.as_ref());

        let pywal = fields.pywal.unwrap_or(false);

        if !path.is_dir() { // normal file
            if let Err(err) = file_render(&mut jinjaenv, &path, target_path, pywal, values) {
                eprintln!("[{warn}] {name}: {err}");
                continue;
            }
            if ! quiet { println!("  * Templated {name} to '{target}'"); }
        } else {
            if ! quiet { println!("  * Templating {name}: directory at '{}'", path.display().italic()); }
            // read directory, encapsulating this into a function and then calling this recursively handle the `recursive` field?
            for i in path.read_dir()? {
                let i = i?;

                let f = &i.file_name();

                let target_path = target_path.join(f);

                if let Err(err) = file_render(&mut jinjaenv, &path.join(f), &target_path, pywal, values) {
                    eprintln!("[{warn}] {name}: {err}");
                    continue;
                }
                if ! quiet { println!("     + {name} {} to '{target}'", &i.path().display(), target = target_path.display().italic()); }
            }
        }
    }

    Ok(())
}

fn parse_srgb(s: &str) -> Result<Srgb<u8>, minijinja::Error> {
    Srgb::<u8>::from_str(s)
        .map_err(|e| minijinja::Error::new(minijinja::ErrorKind::InvalidOperation, format!("{e}")))
}

fn parse_srgba(s: &str) -> Result<Srgba<u8>, minijinja::Error> {
    Srgba::<u8>::from_str(s)
        .map_err(|e| minijinja::Error::new(minijinja::ErrorKind::InvalidOperation, format!("{e}")))
}

pub fn jinja_env<'a>() -> Environment<'a> {
        use minijinja::Error;
        let mut env = Environment::new();
        env.set_keep_trailing_newline(true); // keep the template file intact

        /*filters*/

        // These filters don't require special handling,
        // since they will ignore and don't use alpha whatsoever
        jinjafn!(env, rgb);
        jinjafn!(env, xrgb);
        jinjafn!(env, red);
        jinjafn!(env, green);
        jinjafn!(env, blue);

        /// Blending for usual RRGGBB and RRGGBBAA
        //TODO make this less ugly "but, it werks"
        fn blend(a: String, b: String) -> Result<String, Error> {
            let rgb = parse_srgb(&a);
            let rgba = parse_srgba(&a);

            let rgb1 = parse_srgb(&b);
            let rgba1 = parse_srgba(&b);

            let ret: String = match rgb {
                Ok(o) => {
                    match rgb1 {
                        Ok(o1) => {
                            // SHOULD BE RRGGBB
                            let new = crate::colors::blend(o.into_format(), o1.into_format());
                            let (r, g, b) = new.into_format::<u8>().into_components();
                            format!("#{r:02X}{g:02X}{b:02X}")
                        },
                        Err(_) => {
                            match rgba1 {
                                Ok(o1a) => {
                                    // final output SHOULD BE RRGGBBAA
                                    let new = crate::colors::blend_alpha(o.into_format().into(), o1a.into_format());
                                    let (r, g, b, a) = new.into_format::<u8, u8>().into_components();
                                    format!("#{r:02X}{g:02X}{b:02X}{a:02X}")
                                },
                                Err(_) => {
                                    return Err(minijinja::Error::new(
                                            minijinja::ErrorKind::InvalidOperation,
                                            format!("String '{b}' is not either a hex rgb nor hexa rgba."))
                                    )
                                }
                            }
                        },
                    }
                },
                Err(_) => {
                    match rgba {
                        Ok(oa) => {
                            match rgb1 {
                                Ok(o1) => {
                                    // SHOULD BE RRGGBB
                                    let new = crate::colors::blend((*oa).into_format::<f32>().into(), o1.into_format());
                                    let (r, g, b) = new.into_format::<u8>().into_components();
                                    format!("#{r:02X}{g:02X}{b:02X}")
                                },
                                Err(_) => {
                                    match rgba1 {
                                        Ok(o1a) => {
                                            // final output SHOULD BE RRGGBBAA
                                            let new = crate::colors::blend_alpha(oa.into_format::<f32, f32>().into(), o1a.into_format());
                                            let (r, g, b, a) = new.into_format::<u8, u8>().into_components();
                                            format!("#{r:02X}{g:02X}{b:02X}{a:02X}")
                                        },
                                        Err(_) => {
                                            return Err(minijinja::Error::new(
                                                    minijinja::ErrorKind::InvalidOperation,
                                                    format!("String '{b}' is not either a hex rgb nor hexa rgba."))
                                            )
                                        }
                                    }
                                },
                            }
                        },
                        Err(_) => {
                            return Err(minijinja::Error::new(
                                minijinja::ErrorKind::InvalidOperation,
                                format!("String '{a}' is not either a hex rgb nor hexa rgba."))
                            )
                        },
                    }
                }
            };

            Ok(ret)
        }
        env.add_filter("blend", blend);

        /// Complementary for usual RRGGBB and RRGGBBAA
        fn complementary(s: String) -> Result<String, Error> {
            use crate::colors::Compl;
            let rgb = parse_srgb(&s);
            let rgba = parse_srgba(&s);

            let ret: String = match rgb {
                Ok(o) => {
                    let o: Srgb<f32> = o.into_format();
                    let (r, g, b) = o.complementary().into_format::<u8>().into_components();
                    format!("#{r:02X}{g:02X}{b:02X}")
                },
                Err(_) => {
                    match rgba {
                        Ok(o) => {
                            let o: Srgba<f32> = o.into_format();
                            let (r, g, b, a) = o.complementary().into_format::<u8, u8>().into_components();
                            format!("#{r:02X}{g:02X}{b:02X}{a:02X}")
                        },
                        Err(_) => {
                            return Err(minijinja::Error::new(
                                minijinja::ErrorKind::InvalidOperation,
                                format!("String '{s}' is not either a hex rgb nor hexa rgba."))
                            )
                        },
                    }
                }
            };

            Ok(ret)
        }
        env.add_filter("complementary", complementary);

        /// Saturate function that accepts a RRGGBB or RRGGBBAA
        fn saturate(s: String, arg: f32) -> Result<String, Error> {
            let rgb = parse_srgb(&s);
            let rgba = parse_srgba(&s);

            let ret: String = match rgb {
                Ok(o) => {
                    let o: Hsv = o.into_format::<f32>().into_color();
                    let o: Srgb = o.saturate(arg).into_color();
                    let (r, g, b) = o.into_format::<u8>().into_components();
                    format!("#{r:02X}{g:02X}{b:02X}")
                },
                Err(_) => {
                    match rgba {
                        Ok(o) => {
                            let o: Hsv = o.into_format::<f32, f32>().into_color();
                            let o: Srgba = o.saturate(arg).into_color();
                            let (r, g, b, a) = o.into_format::<u8, u8>().into_components();
                            format!("#{r:02X}{g:02X}{b:02X}{a:02X}")
                        },
                        Err(_) => {
                            return Err(minijinja::Error::new(
                                minijinja::ErrorKind::InvalidOperation,
                                format!("String '{s}' is not either a hex rgb nor hexa rgba."))
                            )
                        },
                    }
                }
            };

            Ok(ret)
        }
        env.add_filter("saturate", saturate);

        /// Darken for usual RRGGBB and RRGGBBAA
        fn darken(s: String, arg: f32) -> Result<String, Error> {
            let rgb = parse_srgb(&s);
            let rgba = parse_srgba(&s);

            let ret: String = match rgb {
                Ok(o) => {
                    let o: Srgb<f32> = o.into_format();
                    let (r, g, b) = o.darken(arg).into_format::<u8>().into_components();
                    format!("#{r:02X}{g:02X}{b:02X}")
                },
                Err(_) => {
                    match rgba {
                        Ok(o) => {
                            let o: Srgba<f32> = o.into_format();
                            let (r, g, b, a) = o.darken(arg).into_format::<u8, u8>().into_components();
                            format!("#{r:02X}{g:02X}{b:02X}{a:02X}")
                        },
                        Err(_) => {
                            return Err(minijinja::Error::new(
                                minijinja::ErrorKind::InvalidOperation,
                                format!("String '{s}' is not either a hex rgb nor hexa rgba."))
                            )
                        },
                    }
                }
            };

            Ok(ret)
        }
        env.add_filter("darken", darken);

        /// Lighten with support for RRGGBBAA aka 'hexa' like values.
        fn lighten(s: String, arg: f32) -> Result<String, Error> {
            let rgb = parse_srgb(&s);
            let rgba = parse_srgba(&s);

            let ret: String = match rgb {
                Ok(o) => {
                    let o: Srgb<f32> = o.into_format();
                    let (r, g, b) = o.lighten(arg).into_format::<u8>().into_components();
                    format!("#{r:02X}{g:02X}{b:02X}")
                },
                Err(_) => {
                    match rgba {
                        Ok(o) => {
                            let o: Srgba<f32> = o.into_format();
                            let (r, g, b, a) = o.lighten(arg).into_format::<u8, u8>().into_components();
                            format!("#{r:02X}{g:02X}{b:02X}{a:02X}")
                        },
                        Err(_) => {
                            return Err(minijinja::Error::new(
                                minijinja::ErrorKind::InvalidOperation,
                                format!("String '{s}' is not either a hex rgb nor hexa rgba."))
                            )
                        },
                    }
                }
            };

            Ok(ret)
        }
        env.add_filter("lighten", lighten);

        /// Strips leading '#' no matter what it is.
        fn strip(hex: String) -> String {
            hex
                .strip_prefix('#')
                .unwrap_or(&hex).to_string()
        }
        env.add_filter("strip", strip);

        /// converts alpha value into a hexadecimal one.
        fn hexa_for_alpha(input: usize) -> Result<String, minijinja::Error> {
            alpha_hexa(input)
                .map_err(|e| minijinja::Error::new(minijinja::ErrorKind::InvalidOperation, e))
        }
        env.add_filter("alpha_hexa", hexa_for_alpha);

        use std::path::PathBuf;

        /// converts alpha value into a hexadecimal one.
        fn basename(p: ViaDeserialize<PathBuf>) -> Result<String, minijinja::Error> {
            let name = p.file_name();
            match name {
                None => Err(minijinja::Error::new(minijinja::ErrorKind::InvalidOperation, "Cannot get basename")),
                Some(s) => Ok(s.to_string_lossy().to_string()),
            }
        }
        env.add_filter("basename", basename);

        env
}

fn jinja_update_alpha(env: &mut Environment, alpha: u8) {
    env.remove_filter("hexa");
    let hexa = move |value: ViaDeserialize<Myrgb>| -> String {
        let a = alpha_hexa(alpha as usize).expect("number from 0..=100 validated by clap");
        Myrgb::hexa(&value, &a)
    };
    env.add_filter("hexa", hexa);
}

impl From<&TemplateFields<'_>> for minijinja::Value {
    fn from(values: &TemplateFields<'_>) -> Self {
        let c = &values.colors;
        let v = minijinja::Value::from_serialize(c);

        context! {
            ..v,
            ..context! {
                alpha      => values.alpha,
                cursor     => c.foreground,
                palette    => values.palette,
                wallpaper  => values.image_path,
                backend    => values.backend,
                colorspace => values.colorspace,
                colors     => c.into_iter().map(|x| x.to_string()).collect::<Vec<String>>(),
            }
        }

    }
}

/// This is used to represent HEXA values, but only the alpha part.
/// Alpha doesn't go as far as 255, only up to a 100, so simple fmt like {:0X} won't do the job.
/// Since [`Myrgb`] type doesn't implement alpha by itself, alpha it's represented separetly.
/// list of hexadecimal alpha values
/// refs:
/// - <https://gist.github.com/lopspower/03fb1cc0ac9f32ef38f4>
/// - <https://net-informations.com/q/web/trans.html>
fn alpha_hexa(input: usize) -> Result<String, &'static str> {
    let alphas_hex = [ "00", "03", "05", "08", "0A", "0D", "0F", "12", "14", "17", "1A", "1C", "1F", "21", "24", "26", "29", "2B", "2E", "30", "33", "36", "38", "3B", "3D", "40", "42", "45", "47", "4A", "4D", "4F", "52", "54", "57", "59", "5C", "5E", "61", "63", "66", "69", "6B", "6E", "70", "73", "75", "78", "7A", "7D", "80", "82", "85", "87", "8A", "8C", "8F", "91", "94", "96", "99", "9C", "9E", "A1", "A3", "A6", "A8", "AB", "AD", "B0", "B3", "B5", "B8", "BA", "BD", "BF", "C2", "C4", "C7", "C9", "CC", "CF", "D1", "D4", "D6", "D9", "DB", "DE", "E0", "E3", "E6", "E8", "EB", "ED", "F0", "F2", "F5", "F7", "FA", "FC", "FF", ];
    let ret = alphas_hex.get(input);
    match ret {
        Some(s) => Ok(s.to_string()),
        None => Err("Input should be in the range of 0 to 100.")
    }
}

/// hash values
impl TemplateFields<'_> {
pub fn to_hash<'a>(&self) -> HashMap<&'a str, String> {
    let mut map = HashMap::new();
    let alpha = self.alpha;
    let col = self.colors;
    let alpha_hex = alpha_hexa(alpha as usize).expect("CANNOT OVERFLOW, validation with clap 0..=100");
    let alpha_dec = f32::from(alpha) / 100.0;

    //XXX instead of multiple `.method()` maybe using enums and match with a single method

    //full path to the image
    map.insert("wallpaper", self.image_path.into());
    map.insert("alpha", alpha.to_string());
    map.insert("alpha_dec", format!("{alpha_dec:.2}"));
    map.insert("alpha_hex", alpha_hex.clone());

    // Include backend, colorspace and filter (palette)
    map.insert("backend", self.backend.to_string());
    map.insert("colorspace", self.colorspace.to_string());
    map.insert("palette", self.palette.to_string());

    // normal output `#EEEEEE`
    map.insert("color0" , col.color0 .to_string());
    map.insert("color1" , col.color1 .to_string());
    map.insert("color2" , col.color2 .to_string());
    map.insert("color3" , col.color3 .to_string());
    map.insert("color4" , col.color4 .to_string());
    map.insert("color5" , col.color5 .to_string());
    map.insert("color6" , col.color6 .to_string());
    map.insert("color7" , col.color7 .to_string());
    map.insert("color8" , col.color8 .to_string());
    map.insert("color9" , col.color9 .to_string());
    map.insert("color10", col.color10.to_string());
    map.insert("color11", col.color11.to_string());
    map.insert("color12", col.color12.to_string());
    map.insert("color13", col.color13.to_string());
    map.insert("color14", col.color14.to_string());
    map.insert("color15", col.color15.to_string());
    map.insert("cursor", col.foreground.to_string());
    map.insert("foreground", col.foreground.to_string());
    map.insert("background", col.background.to_string());

    //.rgb output `235,235,235`
    map.insert("color0.rgb" , col.color0 .rgb());
    map.insert("color1.rgb" , col.color1 .rgb());
    map.insert("color2.rgb" , col.color2 .rgb());
    map.insert("color3.rgb" , col.color3 .rgb());
    map.insert("color4.rgb" , col.color4 .rgb());
    map.insert("color5.rgb" , col.color5 .rgb());
    map.insert("color6.rgb" , col.color6 .rgb());
    map.insert("color7.rgb" , col.color7 .rgb());
    map.insert("color8.rgb" , col.color8 .rgb());
    map.insert("color9.rgb" , col.color9 .rgb());
    map.insert("color10.rgb", col.color10.rgb());
    map.insert("color11.rgb", col.color11.rgb());
    map.insert("color12.rgb", col.color12.rgb());
    map.insert("color13.rgb", col.color13.rgb());
    map.insert("color14.rgb", col.color14.rgb());
    map.insert("color15.rgb", col.color15.rgb());
    map.insert("cursor.rgb", col.foreground.rgb());
    map.insert("foreground.rgb", col.foreground.rgb());
    map.insert("background.rgb", col.background.rgb());

    //.rgba output `235,235,235,1.0`
    map.insert("color0.rgba" , col.color0 .rgba(alpha_dec));
    map.insert("color1.rgba" , col.color1 .rgba(alpha_dec));
    map.insert("color2.rgba" , col.color2 .rgba(alpha_dec));
    map.insert("color3.rgba" , col.color3 .rgba(alpha_dec));
    map.insert("color4.rgba" , col.color4 .rgba(alpha_dec));
    map.insert("color5.rgba" , col.color5 .rgba(alpha_dec));
    map.insert("color6.rgba" , col.color6 .rgba(alpha_dec));
    map.insert("color7.rgba" , col.color7 .rgba(alpha_dec));
    map.insert("color8.rgba" , col.color8 .rgba(alpha_dec));
    map.insert("color9.rgba" , col.color9 .rgba(alpha_dec));
    map.insert("color10.rgba", col.color10.rgba(alpha_dec));
    map.insert("color11.rgba", col.color11.rgba(alpha_dec));
    map.insert("color12.rgba", col.color12.rgba(alpha_dec));
    map.insert("color13.rgba", col.color13.rgba(alpha_dec));
    map.insert("color14.rgba", col.color14.rgba(alpha_dec));
    map.insert("color15.rgba", col.color15.rgba(alpha_dec));
    map.insert("cursor.rgba", col.foreground.rgba(alpha_dec));
    map.insert("foreground.rgba", col.foreground.rgba(alpha_dec));
    map.insert("background.rgba", col.background.rgba(alpha_dec));

    //.xrgba output `ee/ee/ee/ff`
    map.insert("color0.xrgba" , col.color0 .xrgba(&alpha_hex));
    map.insert("color1.xrgba" , col.color1 .xrgba(&alpha_hex));
    map.insert("color2.xrgba" , col.color2 .xrgba(&alpha_hex));
    map.insert("color3.xrgba" , col.color3 .xrgba(&alpha_hex));
    map.insert("color4.xrgba" , col.color4 .xrgba(&alpha_hex));
    map.insert("color5.xrgba" , col.color5 .xrgba(&alpha_hex));
    map.insert("color6.xrgba" , col.color6 .xrgba(&alpha_hex));
    map.insert("color7.xrgba" , col.color7 .xrgba(&alpha_hex));
    map.insert("color8.xrgba" , col.color8 .xrgba(&alpha_hex));
    map.insert("color9.xrgba" , col.color9 .xrgba(&alpha_hex));
    map.insert("color10.xrgba", col.color10.xrgba(&alpha_hex));
    map.insert("color11.xrgba", col.color11.xrgba(&alpha_hex));
    map.insert("color12.xrgba", col.color12.xrgba(&alpha_hex));
    map.insert("color13.xrgba", col.color13.xrgba(&alpha_hex));
    map.insert("color14.xrgba", col.color14.xrgba(&alpha_hex));
    map.insert("color15.xrgba", col.color15.xrgba(&alpha_hex));
    map.insert("cursor.xrgba", col.foreground.xrgba(&alpha_hex));
    map.insert("foreground.xrgba", col.foreground.xrgba(&alpha_hex));
    map.insert("background.xrgba", col.background.xrgba(&alpha_hex));

    //.strip output `EEEEEE`
    map.insert("color0.strip" , col.color0 .strip());
    map.insert("color1.strip" , col.color1 .strip());
    map.insert("color2.strip" , col.color2 .strip());
    map.insert("color3.strip" , col.color3 .strip());
    map.insert("color4.strip" , col.color4 .strip());
    map.insert("color5.strip" , col.color5 .strip());
    map.insert("color6.strip" , col.color6 .strip());
    map.insert("color7.strip" , col.color7 .strip());
    map.insert("color8.strip" , col.color8 .strip());
    map.insert("color9.strip" , col.color9 .strip());
    map.insert("color10.strip", col.color10.strip());
    map.insert("color11.strip", col.color11.strip());
    map.insert("color12.strip", col.color12.strip());
    map.insert("color13.strip", col.color13.strip());
    map.insert("color14.strip", col.color14.strip());
    map.insert("color15.strip", col.color15.strip());
    map.insert("cursor.strip", col.foreground.strip());
    map.insert("foreground.strip", col.foreground.strip());
    map.insert("background.strip", col.background.strip());

    //.red output `235`
    map.insert("color0.red" , col.color0 .red());
    map.insert("color1.red" , col.color1 .red());
    map.insert("color2.red" , col.color2 .red());
    map.insert("color3.red" , col.color3 .red());
    map.insert("color4.red" , col.color4 .red());
    map.insert("color5.red" , col.color5 .red());
    map.insert("color6.red" , col.color6 .red());
    map.insert("color7.red" , col.color7 .red());
    map.insert("color8.red" , col.color8 .red());
    map.insert("color9.red" , col.color9 .red());
    map.insert("color10.red", col.color10.red());
    map.insert("color11.red", col.color11.red());
    map.insert("color12.red", col.color12.red());
    map.insert("color13.red", col.color13.red());
    map.insert("color14.red", col.color14.red());
    map.insert("color15.red", col.color15.red());
    map.insert("cursor.red", col.foreground.red());
    map.insert("foreground.red", col.foreground.red());
    map.insert("background.red", col.background.red());

    //.green output `235`
    map.insert("color0.green" , col.color0 .green());
    map.insert("color1.green" , col.color1 .green());
    map.insert("color2.green" , col.color2 .green());
    map.insert("color3.green" , col.color3 .green());
    map.insert("color4.green" , col.color4 .green());
    map.insert("color5.green" , col.color5 .green());
    map.insert("color6.green" , col.color6 .green());
    map.insert("color7.green" , col.color7 .green());
    map.insert("color8.green" , col.color8 .green());
    map.insert("color9.green" , col.color9 .green());
    map.insert("color10.green", col.color10.green());
    map.insert("color11.green", col.color11.green());
    map.insert("color12.green", col.color12.green());
    map.insert("color13.green", col.color13.green());
    map.insert("color14.green", col.color14.green());
    map.insert("color15.green", col.color15.green());
    map.insert("cursor.green", col.foreground.green());
    map.insert("foreground.green", col.foreground.green());
    map.insert("background.green", col.background.green());

    //.blue output `235`
    map.insert("color0.blue" , col.color0 .blue());
    map.insert("color1.blue" , col.color1 .blue());
    map.insert("color2.blue" , col.color2 .blue());
    map.insert("color3.blue" , col.color3 .blue());
    map.insert("color4.blue" , col.color4 .blue());
    map.insert("color5.blue" , col.color5 .blue());
    map.insert("color6.blue" , col.color6 .blue());
    map.insert("color7.blue" , col.color7 .blue());
    map.insert("color8.blue" , col.color8 .blue());
    map.insert("color9.blue" , col.color9 .blue());
    map.insert("color10.blue", col.color10.blue());
    map.insert("color11.blue", col.color11.blue());
    map.insert("color12.blue", col.color12.blue());
    map.insert("color13.blue", col.color13.blue());
    map.insert("color14.blue", col.color14.blue());
    map.insert("color15.blue", col.color15.blue());
    map.insert("cursor.blue", col.foreground.blue());
    map.insert("foreground.blue", col.foreground.blue());
    map.insert("background.blue", col.background.blue());

    map
}
}
